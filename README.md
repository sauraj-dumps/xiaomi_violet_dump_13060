## violet-user 10 QKQ1.190915.002 V12.5.4.0.QFHCNXM release-keys
- Manufacturer: xiaomi
- Platform: sm6150
- Codename: violet
- Brand: xiaomi
- Flavor: violet-user
- Release Version: 10
- Id: QKQ1.190915.002
- Incremental: V12.5.4.0.QFHCNXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: zh-CN
- Screen Density: 440
- Fingerprint: xiaomi/violet/violet:10/QKQ1.190915.002/V12.5.4.0.QFHCNXM:user/release-keys
- OTA version: 
- Branch: violet-user-10-QKQ1.190915.002-V12.5.4.0.QFHCNXM-release-keys
- Repo: xiaomi_violet_dump_13060


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
